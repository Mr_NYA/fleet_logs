const express = require('express');
const mysql = require('mysql');
var http = require('http');
var PORT = process.env.PORT || 8060;
const app = express();
//creation connection
const db =  mysql.createConnection({
    host : '35.197.203.96',
    user: 'taeva',
    password: 'PovSkT9uk6WBhI0kOxwj'
});


// Connect to MySQL database
db.connect(function(err, db) {  
    if(err){
        throw err;
    }
    console.log('Connected to MySQL database');
 });  



function setNewToken(idDevice, UIDDevice, nomDevice,appId ,channelName ,RTC,RTM,RTM_liaison){
    console.log("date ==> " + new Date());
    console.log("date timestamp ==> " + Date.now());
    let post = {idDevice : idDevice,
                UIDDevice : UIDDevice,
                nomDevice : nomDevice,
                appId : appId,
                channelName: channelName,
                RTC : RTC,
                RTM : RTM,
                RTM_liaison : RTM_liaison,
                Date : new Date(),
                Date_timestamp : Date.now()                
                }

    let sql = 'INSERT INTO FLEET_LOGS.Token SET ?;'
    let query = db.query(sql, post, err => {
        if (err){
            throw err;
        }
        console.log('token added successfully')
    })
}

function setNewTokenWithDate(idDevice, UIDDevice, nomDevice,appId ,channelName ,RTC,RTM,RTM_liaison,Expire_time, Expire_timestamp ){
    console.log("date ==> " + new Date());
    console.log("date timestamp ==> " + Date.now());
    let post = {idDevice : idDevice,
                UIDDevice : UIDDevice,
                nomDevice : nomDevice,
                appId : appId,
                channelName: channelName,
                RTC : RTC,
                RTM : RTM,
                RTM_liaison : RTM_liaison,
                Date : new Date(),
                Date_timestamp : Date.now(),
                Expire_time : Expire_time,
                Expire_timestamp :Date.parse(Expire_timestamp)                 
                }

    let sql = 'INSERT INTO FLEET_LOGS.Token SET ?;'
    let query = db.query(sql, post, err => {
        if (err){
            throw err;
        }
        console.log('token added successfully')
    })
}

var setListeToken = function(req, res){
    console.log("in setListeToken")
    let data = '';
    let listToken;
    req.on('data', chunk => data += chunk)
    
    req.on('end', () => {
        if(data != '' && data != null){
            listToken = JSON.parse(data).listToken;
            console.log('listToken')
            console.table(listToken);
            console.log("list token count ", listToken.length);
            listToken.forEach(tokenDevice => {
                console.log('token RTC', tokenDevice.RTC) 
                setNewToken(tokenDevice.idDevice,
                            tokenDevice.UIDDevice, 
                            tokenDevice.nomDevice, 
                            tokenDevice.appId, 
                            tokenDevice.channelName, 
                            tokenDevice.RTC, 
                            tokenDevice.RTM, 
                            tokenDevice.RTM_liaison
                            )
            }   
            );
            return res.send("Insertion successful")
        }
        else{
            res.send(null);
        }


        
    })
}

var setListeTokenWithDate = function(req, res){
    console.log("in setListeTokenWithDate")
    let data = '';
    let listToken;
    req.on('data', chunk => data += chunk)
    
    req.on('end', () => {
        if(data != '' && data != null){
            listToken = JSON.parse(data).listToken;
            console.log('listTokenWithDate')
            console.table(listToken);
            console.log("list token count ", listToken.length);
            listToken.forEach(tokenDevice => {
                // console.log('token RTC', tokenDevice.RTC) 
                setNewTokenWithDate(tokenDevice.idDevice,
                    tokenDevice.UIDDevice, 
                    tokenDevice.nomDevice, 
                    tokenDevice.appId, 
                    tokenDevice.channelName, 
                    tokenDevice.RTC, 
                    tokenDevice.RTM, 
                    tokenDevice.RTM_liaison, 
                    tokenDevice.Expire_time, 
                    tokenDevice.Expire_timestamp
                    )
            }   
            );
            return res.send("Insertion successful")
        }
        else{
            res.send(null);
        }


        
    })
}


app.post('/setListeToken', setListeToken);

app.post('/setListeTokenWitheDate', setListeTokenWithDate);

http.createServer(app).listen(PORT, function () {
    console.log('FLEET LOGS is Started ' + PORT);
});